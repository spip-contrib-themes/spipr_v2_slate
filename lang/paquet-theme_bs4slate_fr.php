<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-theme_bs4slate
// Langue: fr
// Date: 17-04-2020 16:23:04
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'theme_bs4slate_description' => 'Shades of gunmetal gray',
	'theme_bs4slate_slogan' => 'Shades of gunmetal gray',
);
?>